<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/23/2017
 * Time: 10:37 PM
 */
session_start();

include_once 'vendor/autoload.php';
$image=new \App\Image\Image();
if(isset($_POST['insert'])){
     $name=$_POST['name'];


    $filename=$_FILES['image']['name'];
    $filelocation=$_FILES['image']['tmp_name'];
    $div=explode('.',$filename);

    $fileExtension=strtolower(end($div));
    $uniqueName=substr(md5(time()), 0, 10).'.'.$fileExtension;
    $uploaded_image = "upload/".$uniqueName;
    move_uploaded_file($filelocation,$uploaded_image);


    $image->setName($name);
    $image->setImageFile($uploaded_image);

    $image->insertImage();

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <?php
        if(isset($_SESSION['insert'])){
            echo"<div class='alert alert-success'>Image uploaded !!</div>";
            session_unset();
        }
        ?>
        <a href="view/view.php">View</a>
        <div class="col-md-6">
            <form method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="form-group">
                    <label for="image">Image:</label>
                    <input type="file" class="form-control" name="image">
                </div>
                <button type="submit" class="btn btn-info" name="insert">Submit</button>
            </form>
        </div>
    </div>

</div>

</body>
</html>
