<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/23/2017
 * Time: 11:39 PM
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/23/2017
 * Time: 10:37 PM
 */
session_start();

include_once '../vendor/autoload.php';
$id=$_GET['id'];
$image=new \App\Image\Image();
$image->setId($id);
$res=$image->edit();

if(isset($_POST['insert'])){

    //unlink("../".$res['imageFile']);
    $name=$_POST['name'];

    $filename=$_FILES['image']['name'];

    if($filename==''){
        $uploaded_image=$res['imageFile'];
        $image->setImageFile($uploaded_image);
    }else{
        $filelocation=$_FILES['image']['tmp_name'];
        $div=explode('.',$filename);
        $fileExtension=strtolower(end($div));
        $uniqueName=substr(md5(time()), 0, 10).'.'.$fileExtension;
        $uploaded_image = "upload/".$uniqueName;
        unlink("../".$res['imageFile']);
        $image->setImageFile($uploaded_image);
        move_uploaded_file($filelocation,"../".$uploaded_image);
    }

    $image->setName($name);
    $image->update();
    
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <?php
        if(isset($_SESSION['update'])){
            echo"<div class='alert alert-success'>Data updated !!</div>";
            session_unset();
        }
        ?>
        <a href="view/view.php">View</a>
        <div class="col-md-6">
            <form method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value="<?php echo $res['name']?>">
                </div>
                
                <div class="form-group">
                    Image:
                    <img src="../<?php echo $res['imageFile']?>" alt="" style="width: 80px;height: 80px">
                </div>
                
                <div class="form-group">
                    <label for="image">Image:</label>
                    <input type="file" class="form-control" name="image">
                </div>
                <button type="submit" class="btn btn-info" name="insert">Submit</button>
            </form>
        </div>
    </div>

</div>

</body>
</html>

