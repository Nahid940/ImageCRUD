<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/23/2017
 * Time: 11:26 PM
 */
include_once '../vendor/autoload.php';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="row">
    <div class="container">
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Image</th>
                <th>Action</th>
            </tr>

            <?php
                $imageview=new \App\Image\Image();

                foreach ($imageview->viewData() as $images){
            ?>
            <tr>
                <td><?php echo $images['name']?></td>
                <td><img src="../<?php echo $images['imageFile']?>"  style="width: 50px;height: 50px" alt=""></td>

                <td>
                    <a href="edit.php?id=<?php echo $images['id']?>" class="btn btn-info">Edit</a>
                </td>
            </tr>
            <?php }?>
        </table>
    </div>
</div>

</body>
</html>
