<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/23/2017
 * Time: 10:40 PM
 */
namespace App\Image;
use App\DB;

class Image
{
    private $id;
    private $name;
    private $imageFile;

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $imageFile
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


//    private function set($data){
//        if(array_key_exists('name',$data)){
//            $this->name=$data['name'];
//        }
//
//        if(array_key_exists('imageFile',$data)){
//            $this->name=$data['imageFile'];
//        }
//    }



    public function insertImage(){
        $sql="insert into imagecrud (name,imageFile) VALUES (:name,:imageFile)";
        $stmt=DB::myQuery($sql);
        $stmt->bindValue(':name',$this->name);
        $stmt->bindValue(':imageFile',$this->imageFile);

        if($stmt->execute()){
            $_SESSION['insert']="Inserted";
        }
    }

    public function viewData()
    {
        $sql = "select * from imagecrud";
        $stmt = DB::myQuery($sql);
        $stmt->execute();
        $res=$stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $res;
    }

    public function edit()
    {
        $sql = "select * from imagecrud WHERE id=:id";
        $stmt = DB::myQuery($sql);
        $stmt->bindValue(':id',$this->id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);

    }

    public function update()
    {
        $sql = "update imagecrud set name=:name, imageFile=:imageFile WHERE id=:id";
        $stmt = DB::myQuery($sql);
        $stmt->bindValue(':id',$this->id);
        $stmt->bindValue(':name',$this->name);
        $stmt->bindValue(':imageFile',$this->imageFile);


        if( $stmt->execute()){
            $_SESSION['update']="Updated";
        }
    }

}