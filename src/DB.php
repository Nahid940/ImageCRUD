<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/23/2017
 * Time: 10:44 PM
 */
namespace App;

class DB
{

    private static $pdo;

    public static function myCon(){
        try{
            self::$pdo=new \PDO('mysql:host=localhost;dbname=imagecrud',"root","");
        }catch (\PDOException $exp){
            return $exp->getMessage();
        }
        return self::$pdo;

    }

   public static function myQuery($query){
        return self::myCon()->prepare($query);
   }
}